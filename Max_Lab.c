//This is the main program of Max's part

#include "simpletools.h"                      // Include simpletools
#include "adcDCpropab.h"                      // Include adcDCpropab                         

int main();
void startMessage();
int decideInput();
int checkSensor();
int output();

int main()
{
    startMessage();
    int choice = 0;
    int armed = 0;
    int time = 0;
    print("\n1) Motion Sensor\n2) Hall Effect Sensor\n3) Break Beam Sensor\n");
    choice = decideInput(time);
    print("You chose #%d",choice);
    pause(2000);
    print("%c",CLS);
    while (!armed) {armed = checkSensor(choice, armed);}              
    output();
}

int decideInput(int time)
{   
    adc_init(21, 20, 19, 18);                     // CS=21, SCL=20, DO=19, DI=18
    int v, decision = 0;
    while(!decision)                              // Loop repeats indefinitely
    {    
        v = 0; 
        decision = 0;
        decision = input(0);                      // button
        v = 4.3 - adc_volts(3);                   // Check A/D 3
        putChar(HOME);                            // Cursor -> top-left "home"
        print("Choice = %d%c\n", v, CLREOL);      // Display choice
        pause(100);                               // Wait 1/10 s
    }
    print("%c",CLS);
    if (time == 0) {return v;}
    else if (time == 1) 
    {
        if (v <= 2) {return 0;}
        else {return 1;}
    }
    else {decideInput(time);}
}

int checkSensor(int choice, int armed)
{
    // These are all temporarily set to the button for testing
    if (choice == 1) {return input(1);}           // Motion sensor
    else if (choice == 2) {return input(0);}      // Hall effect sensor
    else if (choice == 3) {return (1 - input(5));}      // Break beam sensor
}                      

int output()
{
    low(27);
    for (int i = 0; i <= 2; i++)
    { 
        high(26);
        pause(500);
        low(26);
        pause(500);
        
    }
    print("\nProgram over. Thank you for choosing Super Security 2000!\n");
    char str[3];
    print("Would you like to run again? (1,2 = Yes | 3,4 = No) ");
    int time = 1;
    int choice;
    choice = decideInput(time);
    print("%c",CLS); 
    if (choice == 0) {main();}
    else 
    {
        print("Program Terminated");
        pause(1000); 
        print("%c",CLS);
    }    
}

void startMessage()
{
    print("Welcome to Super Security 2000!\n");
    pause(200);
    print("Starting");
    pause(50);
    print(".");
    pause(50);
    print(".");
    pause(50);
    print(".");
    pause(50);
    print("%c",CLS);
}